class Seller < ApplicationRecord
  has_secure_password

  validates :name,:password, presence: true, length: { minimum: 5 }
  validates :mobile_no, presence: true, length: { minimum: 10 },uniqueness: true, numericality: { only_integer: true }
  validates :email_id, presence: { message: "must be given please" }, length: { minimum: 6 }
end
