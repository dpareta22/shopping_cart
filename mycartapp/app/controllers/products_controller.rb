class ProductsController < ApplicationController
  #http_basic_authenticate_with name: "dj", password: "123", except: [:home, :details]
  #http_basic_authenticate_with name: "dj", password: "1234", only: :destroy
  def home
    @products = Product.all
  end

  def details
    @product = Product.find(params[:id])
  end

  def show
    @product = Product.find(params[:id])
  end

  def new
    @product = Product.new
  end

  def create
    @product = Product.new(product_params)

    if  @product.save
        redirect_to @product
    else
        render 'new'
    end
  end

private
  def product_params
    params.require(:product).permit(:name,:details,:quantity,:price)
  end
end
