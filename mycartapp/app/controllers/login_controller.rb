class SessionsController < ApplicationController
  
  def identity
  end

  def logindetails
  end

  def create
    @seller = Seller.where(email_id: params[:email_id]).first
    
    if @seller && @seller.authenticate(params[:password])
     session[:seller_id] = @seller.id
     redirect_to products_home_path, :notice => "Logged in successfully"
    else
     flash[:danger] = 'Not authenticated'
     render :action => 'new'
    end
  end
  
  def destroy
    session[:user_id] = nil
    redirect_to login_logindetails_path, :notice => "You successfully logged out"
  end

end
