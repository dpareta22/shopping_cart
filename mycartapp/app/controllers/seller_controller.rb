class SellerController < ApplicationController
  def dashboard
    @products = Product.all
  end
  
  def show
    @seller = Seller.find(params[:id])
  end

  def new
    @seller = Seller.new
  end

  def create
    @seller = Seller.new(seller_params)
    if @seller.save
      redirect_to seller_dashboard_path
    else
      render 'new'
    end
  end

private
  def seller_params
    params.require(:seller).permit(:name,:email_id,:mobile_no,:password)
  end
end
