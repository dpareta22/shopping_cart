class CreateSellers < ActiveRecord::Migration[5.2]
  def change
    create_table :sellers do |t|
      t.string :name
      t.string :email_id
      t.integer :mobile_no
      t.string :password

      t.timestamps
    end
  end
end
