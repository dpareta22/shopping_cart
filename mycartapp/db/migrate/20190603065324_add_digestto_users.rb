class AddDigesttoUsers < ActiveRecord::Migration[5.2]
  def change
    remove_column :sellers, :password
    add_column :sellers, :password, :digest
  end
end
