Rails.application.routes.draw do

  get 'products/home'
  get 'products/details'
  get 'products/new'
  get 'seller/dashboard'
  match 'seller/new', via: [:get, :post]
  post 'seller/create'
  get 'seller/show'
  get 'login/identity'
  get 'login/logindetails'
  post 'login/create'
  get 'login/login'

  resources :users
  resources :product
  root 'login#identity'
end
