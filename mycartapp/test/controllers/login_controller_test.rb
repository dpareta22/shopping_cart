require 'test_helper'

class LoginControllerTest < ActionDispatch::IntegrationTest
  test "should get identity" do
    get login_identity_url
    assert_response :success
  end

end
