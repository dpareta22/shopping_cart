Rails.application.routes.draw do

  devise_for :users, controllers: { sessions: 'users/sessions' }
  resources :products 
  get 'products/home'

  root 'products#home'
end
