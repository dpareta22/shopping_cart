class UsersController < ApplicationController
  
  def show
    redirect_to new_session_path
  end

  def new
    @user = User.new
  end

  def dashboard

  end

  def create
    @user = User.new(user_params)
    if @user.save
      redirect_to @user, notice: 'User was successfully created.'
    else
      render 'new'
    end
  end

  private
  def user_params
    params.require(:user).permit(:name, :email, :password, :password_confirmation)
  end
end
