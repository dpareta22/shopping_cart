class User < ApplicationRecord
  has_secure_password
  validates :name,:password, presence: true, length: { minimum: 5 }
  validates :email, presence: { message: "must be given please" }, length: { minimum: 6 }

end
