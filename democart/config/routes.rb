Rails.application.routes.draw do
  
  root 'users#new'

  
  resources :users do
    member do
      get 'dashboard'
    end
  end

  resources :sessions, only: [:new, :create, :destroy]

  get 'login' => 'sessions#new', as: 'login'
  get 'signup' => 'users#new', as: 'signup'
  get 'logout' => 'sessions#destroy', as: 'logout'

end
